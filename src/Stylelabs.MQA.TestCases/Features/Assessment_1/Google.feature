﻿
Feature: Search on Google.com
	I want to preform a search on Google on my next holidays destination

Scenario Outline: Looking for a travel destination
Given I navigate to Google
When I insert the text Bahamas in the search box
And I click on the key Enter
Then I am on the search result page
And Save a screenshot

    Examples:
    | searchText |
    | Bahamas    |
    | New York   |

