﻿Feature: Expedia
	In order to avoid silly mistakes
	As a math idiot
	I want to be told the sum of two numbers

@mytag
Scenario: Add two numbers
	Given I navigate to Expedia
	When I insert the text Brussels, Belgium (BRU-All Airports) in the Origin Airport text box
	And I insert the text New York, New York in the Destination Airport text box
	And I insert the text 09/01/2017 in the Departure Date text box
	And I insert the text 09/02/2017 in the Returning Date text box
	And I select 1 in adults dropdown
	And I select 1 in children dropdown
	And I select 3 in the first child age dropdown
	Then Click Search Button
	And Check that results are shown
