﻿using NUnit.Framework;
using OpenQA.Selenium;
using Stylelabs.MQA.Core;
using Stylelabs.MQA.PageElements.Pages;
using System.Collections.Generic;
using TechTalk.SpecFlow;

namespace Stylelabs.MQA.TestCases.Steps
{
    [Binding]
    public class Expedia
    {
        private IWebDriver _driver = Base.Driver;
        private ExpediaPage _expediaPage = new ExpediaPage();

        [Given(@"I navigate to Expedia")]
        public void GivenINavigateToExpedia()
        {
            //I navigate to the google page URL
            _expediaPage.NavigateTo();

            //And I verify that I'm on the correct page checking the page title
            string pageTitle = _driver.Title;
            Assert.AreEqual("Expedia", pageTitle);
        }

        [When(@"I insert the text (.*) in the Origin Airport text box")]
        public void WhenIInsertTheTextInTheOriginAirportInput(string originAirport)
        {
            _expediaPage.setOriginAirport(originAirport);
        }

        [When(@"I insert the text (.*) in the Destination Airport text box")]
        public void WhenIInsertTheTextInTheDestinationAirportInput(string destinationAirport)
        {
            _expediaPage.setDestinationAirport(destinationAirport);
        }

        [When(@"I insert the text (.*) in the Departure Date text box")]
        public void WhenIInsertTheTextInTheDepartingInput(string departureDate)
        {
            _expediaPage.setDepartureDate(departureDate);
        }

        [When(@"I insert the text (.*) in the Returning Date text box")]
        public void WhenIInsertTheTextInTheReturningDateInput(string returningDate)
        {
            _expediaPage.setReturningDate(returningDate);
        }

        [When(@"I select (.*) in adults dropdown")]
        public void WhenISelectAdultsNumber(int adultsNumber)
        {
            _expediaPage.setNumberOfAdults(adultsNumber);
        }

        [When(@"I select (.*) in children dropdown")]
        public void WhenISelectChildrenNumber(int childrenNumber)
        {
            _expediaPage.setNumberOfChildren(childrenNumber);
        }

        [When(@"I select (.*) in the first child age dropdown")]
        public void WhenISelectChildrAge(int childAge)
        {
            _expediaPage.setChildAge(childAge);
        }

        [Then(@"Click Search Button")]
        public void ThenIClickSearchButton()
        {
            _expediaPage.clickSearchButton();
        }

        [Then(@"Check that results are shown")]
        public void ThenIAmSavingAScreenshot()
        {
            _expediaPage.assertResultsAreShown();
        }
    }
}
