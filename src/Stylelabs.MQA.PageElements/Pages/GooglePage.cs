﻿
using System.Drawing.Imaging;
using NUnit.Framework;
using OpenQA.Selenium;
using Stylelabs.MQA.Core;


namespace Stylelabs.MQA.PageElements.Pages
{
    public class GooglePage : PageBase
    {
        private static readonly string _homePageUrl = "https://www.google.com";
        private static string _searchtxtID = "lst-ib";
        private static string _resultStatsID = "resultStats";
        private static string _resultsId = "ires";
        private static string _pathToSaveScreenshot = @".\\..\\..\\..\\Test_output\\result";

        public GooglePage() : base(_homePageUrl) { }

        public void insertTextInSearchBox(string searchValue)
        {
            IWebElement _searchBoxLocator = Base.Driver.FindElement(By.Id(_searchtxtID));
            _searchBoxLocator.SendKeys(searchValue);
        }

        public void clickEnterKeyInSearchBox ()
        {
            IWebElement _searchBoxLocator = Base.Driver.FindElement(By.Id(_searchtxtID));
            _searchBoxLocator.SendKeys(Keys.Enter);
            Base.Driver.Manage().Timeouts().ImplicitWait = System.TimeSpan.FromSeconds(2);
         }

        public void assertResultStatIsDisplayed()
        {
            IWebElement _resultStatsLocator = Base.Driver.FindElement(By.Id(_resultStatsID));
            Assert.IsTrue(_resultStatsLocator.Text.Contains("results"),"I'm not on the result page");
        }

        public void assertResultsAreShown()
        {
            IWebElement _results = Base.Driver.FindElement(By.Id(_resultsId));
            Assert.IsTrue(_results.Displayed, "Results are shown");
        }

        public void saveScreenshot()
        {
            Screenshot ss = ((ITakesScreenshot)Base.Driver).GetScreenshot();
            ss.SaveAsFile(_pathToSaveScreenshot, ScreenshotImageFormat.Png);
        }
    }
}
