﻿using NUnit.Framework;
using OpenQA.Selenium;
using Stylelabs.MQA.Core;
using System;

namespace Stylelabs.MQA.PageElements.Pages
{
    public class ExpediaPage : PageBase
    {
        private static readonly string _homePageUrl = "https://www.expedia.com/";
        private static string _originAirportId = "package-origin-hp-package-airport_code";
        private static string _destinationAirportId = "package-destination-hp-package-airport_code";
        private static string _departureInputId = "package-departing-hp-package";
        private static string _returningInputId = "package-returning-hp-package";
        private static string _adultsDropdownId = "package-1-adults-hp-package";
        private static string _childrenDropdownId = "package-1-children-hp-package";
        private static string _childAgeDropdownId = "package-1-age-select-1-hp-package";
        private static string _searchButtonId = "search-button-hp-package";
        private static string _resultContainer = "resultsContainer";
        private static string _optionCssSelectorTemplate = "option[value='{0}']";

        public ExpediaPage() : base(_homePageUrl) { }

        public void setOriginAirport(string originAirport)
        {
            IWebElement _originAirportLocator = Base.Driver.FindElement(By.Id(_originAirportId));
            _originAirportLocator.SendKeys(originAirport);
        }

        public void setDestinationAirport(string destinationAirport)
        {
            IWebElement _destinationAirportLocator = Base.Driver.FindElement(By.Id(_destinationAirportId));
            _destinationAirportLocator.SendKeys(destinationAirport);
        }

        public void setDepartureDate(string departureDate)
        {
            IWebElement _departureLocator = Base.Driver.FindElement(By.Id(_departureInputId));
            _departureLocator.SendKeys(departureDate);
        }

        public void setReturningDate(string returningDate)
        {
            IWebElement _returningLocator = Base.Driver.FindElement(By.Id(_returningInputId));
            _returningLocator.SendKeys(returningDate);
        }

        public void setNumberOfAdults(int numberOfAdults)
        {
            IWebElement _adultDropdownLocator = Base.Driver.FindElement(By.Id(_adultsDropdownId));
            var cssSelecttor = String.Format(_optionCssSelectorTemplate, _adultsDropdownId);
            _adultDropdownLocator.FindElement(By.CssSelector(cssSelecttor)).Click();

        }

        public void setNumberOfChildren(int numberOfChildren)
        {
            IWebElement _childrenDropdownLocator = Base.Driver.FindElement(By.Id(_childrenDropdownId));
            var cssSelecttor = String.Format(_optionCssSelectorTemplate, numberOfChildren);
            _childrenDropdownLocator.FindElement(By.CssSelector(cssSelecttor)).Click();

        }

        public void setChildAge(int childAge)
        {
            IWebElement _childAgeDropdownLocator = Base.Driver.FindElement(By.Id(_childAgeDropdownId));
            var cssSelecttor = String.Format(_optionCssSelectorTemplate, childAge);
            _childAgeDropdownLocator.FindElement(By.CssSelector(cssSelecttor)).Click();

        }

        public void clickSearchButton()
        {
            IWebElement _searchButton = Base.Driver.FindElement(By.Id(_searchButtonId));
            _searchButton.Click();

        }

        public void assertResultsAreShown()
        {
            new OpenQA.Selenium.Support.UI.WebDriverWait(Base.Driver, System.TimeSpan.FromSeconds(10)).
                Until(OpenQA.Selenium.Support.UI.ExpectedConditions.ElementExists((By.Id(_resultContainer))));
            IWebElement _results = Base.Driver.FindElement(By.Id(_resultContainer));
            Assert.IsTrue(_results.Displayed, "Results are shown");
        }
    }
}
